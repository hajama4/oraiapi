## Description
Create a service, which returns product recommendations depending on the weather forecast.

## Technology stack

PHP 7.4.9 version;
MySQL database;
Symfony framework;


## Getting Started

### Clone the repository
```bash
$ git clone https://hajama4@bitbucket.org/hajama4/oraiapi.git
```

### Change directory
```bash
$ cd oraiapi
```

### Use composer to manage and install dependencies
```bash
$ composer install
```

### Start the application
#### Backend
My personaly used wampserver64 as host
```bash
$ php bin/console server:run
```
## Create database
```bash
Database name 'orai'

```
## Launch database migrations
```bash
$ php bin/console doctrine:migrations:migrate
```
## Add data to database
No faker generated data :(

```bash
INSERT INTO `weather_conditions` (`id`, `name`, `is_active`) VALUES
(1, 'clear', 1),
(2, 'isolated-clouds', 1),
(3, 'scattered-clouds', 1),
(4, 'overcast', 1),
(5, 'light-rain', 1),
(6, 'moderate-rain', 1),
(7, 'heavy-rain', 1),
(8, 'sleet', 1),
(9, 'light-snow', 1),
(10, 'moderate-snow', 1),
(11, 'heavy-snow', 1),
(12, 'fog', 1),
(13, 'unknown', 1);

INSERT INTO `product` (`id`, `name`, `sku`, `price`, `weather`, `min_temperature`, `max_temperature`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Black coat', 'BC-1', '100.10', 1, '-5.0', '5.0', 1, '2021-02-03 15:34:25', '2021-02-03 15:34:25'),
(2, 'Black fur cap', 'BFC-1', '100.10', 1, '-5.0', '5.0', 1, '2021-02-03 15:34:25', '2021-02-03 15:34:25'),
(3, 'Black leather gloves', 'BLG-1', '100.10', 1, '-5.0', '5.0', 1, '2021-02-03 15:34:25', '2021-02-03 15:34:25'),
(4, 'Umbrella', 'UB-1', '100.10', 5, '-5.0', '5.0', 1, '2021-02-03 15:34:25', '2021-02-03 15:34:25'),
(5, 'Fur boots', 'FB-1', '15.00', 10, NULL, NULL, 1, '2021-02-09 14:24:20', '2021-02-09 14:24:20'),
(6, 'Fur boots', 'FB-1', '15.00', 10, NULL, NULL, 1, '2021-02-09 14:24:24', '2021-02-09 14:24:24');
```

## Usage examples
Basically in order to get json data you need to hit
```bash
http://orai/public/products/recommended/:cityName

example: http://orai/public/products/recommended/Vilnius
```
Json data should look like this

```bash
{
    "city": "Vilnius",
    "recommendations": [
        {
            "weatherCondition": "light-snow",
            "forecastDate": "2021-02-09",
            "products": []
        },
        {
            "weatherCondition": "light-rain",
            "forecastDate": "2021-02-09",
            "products": [
                {
                    "id": 4,
                    "name": "Umbrella",
                    "sku": "UB-1",
                    "price": "100.10",
                    "minTemperature": "-5.0",
                    "maxTemperature": "5.0",
                    "active": true,
                    "createdAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updatedAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    }
                }
            ]
        },
        {
            "weatherCondition": "clear",
            "forecastDate": "2021-02-09",
            "products": [
                {
                    "id": 1,
                    "name": "Black coat",
                    "sku": "BC-1",
                    "price": "100.10",
                    "minTemperature": "-5.0",
                    "maxTemperature": "5.0",
                    "active": true,
                    "createdAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updatedAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    }
                },
                {
                    "id": 2,
                    "name": "Black fur cap",
                    "sku": "BFC-1",
                    "price": "100.10",
                    "minTemperature": "-5.0",
                    "maxTemperature": "5.0",
                    "active": true,
                    "createdAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updatedAt": {
                        "date": "2021-02-03 15:34:25.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    }
                }
            ]
        }
    ]
}
```

## Notification about LHMT API usage can be found
```bash
http://orai/public
```