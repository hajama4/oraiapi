<?php


namespace App\Controller;

use App\Helper\Product\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    private $productService;

    public function __construct(Product $ps)
    {
        $this->productService = $ps;
    }

    /**
     * @param Request $request
     * @param string $city
     * @return Response
     * @Route("/products/recommended/{city}", name="product_recomended")
     */
    public function index(Request $request, $city)
    {
        $product = $this->productService;
        $products = $product->getResults($city);
        $response =  new Response($products, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
        $response->setPublic();
        $response->setMaxAge(300);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}