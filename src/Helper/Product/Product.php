<?php

namespace App\Helper\Product;

use App\Helper\Product\ResultFormer;

class Product implements ProductInterface
{
    /**
     * @var ResultFormer
     */
    private $resultFormer;

    public function __construct(ResultFormer $resultFormer)
    {
        $this->resultFormer = $resultFormer;
    }

    /**
     * @param string $city
     * @return array
     */
    public function getResults($city)
    {
        $jsonContent = '';
        if (!is_null($city)) {
            $forecastUrl = "https://api.meteo.lt/v1/places/" . $city . "/forecasts/long-term";
            $forecastJson = file_get_contents($forecastUrl);
            $weatherForecast = json_decode($forecastJson);
            $groupedForecast = $this->resultFormer->groupForecast($weatherForecast);
            $products = $this->resultFormer->getProducts($groupedForecast, $city);
            $jsonContent = json_encode($products, JSON_PRETTY_PRINT);
        }

        return $jsonContent;
    }
}