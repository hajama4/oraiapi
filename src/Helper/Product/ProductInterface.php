<?php


namespace App\Helper\Product;

interface ProductInterface
{
    public function getResults($city);
}