<?php


namespace App\Helper\Product;

use Doctrine\ORM\EntityManagerInterface;

class ResultFormer implements ResultFormerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $productRepository;
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->productRepository = $entityManager->getRepository('App:Product');
    }

    /**
     * @param $weatherForecast
     * @return array
     */
    public function groupForecast($weatherForecast)
    {
        $groups = [];
        $currentDate = $weatherForecast->forecastCreationTimeUtc;
        $checkUntil = date('Y-m-d', strtotime($currentDate. ' + 2 days'));
        foreach($weatherForecast->forecastTimestamps as $i) {
            $date = date('Y-m-d', strtotime($i->forecastTimeUtc));
            if ($date > $checkUntil) {
                break;
            }
            $groups[$date][$i->conditionCode] = $i;
        }

        return $groups;
    }

    /**
     * @param array $groupedForecast
     * @param string $city
     * @return array
     */
    public function getProducts($groupedForecast, $city)
    {
        $results = [];
        foreach($groupedForecast as $i => $gf) {
            foreach($gf as $f) {
                $products = $this->productRepository->findByWeatherCondition($f->conditionCode);
                $results[] =
                    [
                        'weatherCondition' => $f->conditionCode,
                        'forecastDate' => $i,
                        'products' => $products
                    ];
            }
        }
        $results = [
            'city' => $city,
            'recommendations' => $results
        ];

        return $results;
    }
}