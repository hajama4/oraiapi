<?php


namespace App\Helper\Product;


interface ResultFormerInterface
{
    public function groupForecast($weatherForecast);

    public function getProducts($groupedForecast, $city);
}